import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';

const styles = {
  root: {
    width: 500,
  },
};

const SimpleBottomNavigation=({classes, filterValue, changeFilter})=>(
      <BottomNavigation
        value={filterValue}
        onChange={(event, value) => {
          changeFilter(value);
        }}
        showLabels
        className={classes.root}
      >
        <BottomNavigationAction label="All" icon={<RestoreIcon />} />
        <BottomNavigationAction label="Active" icon={<FavoriteIcon />} />
        <BottomNavigationAction label="Completed" icon={<LocationOnIcon />} />
        <BottomNavigationAction label="Deleted" icon={<LocationOnIcon />} />
      </BottomNavigation>
    );


export default withStyles(styles)(SimpleBottomNavigation);