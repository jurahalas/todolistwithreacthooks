import { useState } from 'react';

export default initialValue => {
  const [allTodos, setAllTodos] = useState(initialValue);
  const [filterValue, setFilter] = useState(0);
  const [todos, setTodos] = useState(allTodos);
  const [deletedTodos, setDeletedTodos] = useState([]);

  return {
    todos,
    addTodo: todoText => {
      setAllTodos([...allTodos, {id: allTodos.length, text: todoText, completed: false}]);
      setTodos([...allTodos, {id: allTodos.length, text: todoText, completed: false}]);
    },
    deleteTodo: todoIndex => {
      const newTodos = todos
        .filter((el) => el.id !== todoIndex);

      const deletedTodo = todos
        .filter((el) => el.id === todoIndex);
      const deletedTodosValues = [...deletedTodos,...deletedTodo];
debugger;
      setTodos(newTodos);
      setAllTodos(newTodos);
      setDeletedTodos(deletedTodosValues)
    },
    completedItem: todoIndex => {
      const newTodos = allTodos.map((el)=>
        (el.id === todoIndex)
        ? {...el, completed: el.completed === false }
        : el
      );

      setAllTodos(newTodos);
      setTodos(newTodos);
    },
    filterValue,
    changeFilter: value => {
      setFilter(value);
      const newTodos = allTodos
        .filter((el, index) =>
         /* switch(value){
            case 0 :
              return el.completed
            case 1:
              return !el.completed
            case 2:
              return index !== todoIndex
          }*/

           !el.completed);

      const completedTodos = allTodos
        .filter((el, index) =>el.completed);
debugger;
if(value === 0){
  setTodos(allTodos);
}
      if(value === 1){
  setTodos(newTodos);
}
      if(value === 2){

  setTodos(completedTodos);
}
      if(value === 3){

        setTodos(deletedTodos);
      }

    }
  };
};