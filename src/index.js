import React from 'react';
import ReactDOM from 'react-dom';
import Typography from '@material-ui/core/Typography';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import useTodoState from './useTodoState';
import Navigation from './bottomNavigation/Navigation';
import './index.css';

const App = () => {
  const { todos, addTodo, deleteTodo, completedItem, filterValue, changeFilter } = useTodoState([]);
debugger;
  return (
    <div className="App">
      <Typography variant="h2">
        Todos
      </Typography>

      <TodoForm
        saveTodo={todoText => {
          const trimmedText = todoText.trim();

          if (trimmedText.length > 0) {
            addTodo(trimmedText);
          }
        }}
      />

      <TodoList todos={todos} deleteTodo={deleteTodo} completedItem={completedItem}/>
      <Navigation filterValue={filterValue} changeFilter={changeFilter}/>
    </div>
  );
};

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);